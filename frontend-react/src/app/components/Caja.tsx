import Image from "next/image";
interface CajaProps{
    image: string;
    paragramph: string;
}

const Caja = ({image, paragramph} :CajaProps)=>{
    return (
        <div className="caja-props">
            <Image src={image} alt="" width={22} height={22}/>
            <p>{paragramph}</p>
        </div>
    )
}
export default Caja;