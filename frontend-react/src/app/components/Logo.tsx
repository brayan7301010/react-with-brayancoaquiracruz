interface LogoProps {
    leter:string;
    leter2:string;
}

const Logo = ({leter, leter2} :LogoProps)=>{
    return (
        <div className="logo">
            <h2 className="uno">{leter}</h2>
            <h2 className="dos">{leter2}</h2>
        </div>
    )
}
export default Logo;