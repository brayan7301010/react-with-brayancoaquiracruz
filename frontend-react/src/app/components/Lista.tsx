interface ListaProps {
    title:string;
    components1:string;
    components2:string;
    components3:string;
    components4:string;
}

const Lista = ({title, components1,components2,components3,components4} :ListaProps)=>{
    return (
        <div className="Lista">
            <ul className="lista-interma">
                <li className="primary-lists" >{title}</li>
                <li className="secondary-list">{components1}</li>
                <li className="secondary-list">{components2}</li>
                <li className="secondary-list">{components3}</li>
                <li className="secondary-list">{components4}</li>
            </ul>
        </div>
    )
}
export default Lista;