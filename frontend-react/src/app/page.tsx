import Image from "next/image";
import styles from "./page.module.css";
import Link from "next/link";
import Logo from "./components/Logo";
import FeatherIcon from "feather-icons-react";
import { Button, TextField } from "@mui/material";
import { IconPropsSizeOverrides } from "@mui/material";
import ArrowRightAltIcon from '@mui/icons-material/ArrowRightAlt';
import ExploreIcon from '@mui/icons-material/Explore';
import Caja from "./components/Caja";
import Lista from "./components/Lista";

export default function Home() {
  return (
    <div className="Principal">
      <div className="Header">
        <div className="Nav">
          <ul className="links">
            <li>
              <Link href="/.">Home</Link>
            </li>
            <li>
              <Link href="/.">Catalog</Link>
            </li>
            <li>
              <Link href="/.">Contact</Link>
            </li>
            <li>
              <Link href="/.">Features</Link>
            </li>
          </ul>
          <Logo leter={"Holo"} leter2={"Gaze"}></Logo>
          <div className="busqueda">
            <Image src="/imagens/busqueda.svg" alt="" width={336} height={46}/> 
            <Image src="/imagens/icons.svg" alt="" width={22} height={22}/>  
          </div>
        </div>
        <div className="Content">
          <div className="present">
            <div className="inicio">
              <h3>Virtual Headsets</h3>
            </div>
            <div className="titulo">
              <h1>Experience a new dimension of reality</h1>
            </div>
            <div className="parrafo">
              <Image src="/imagens/estrella.svg" alt="" width={22} height={22} />
              <p>Step into the future with our virtual headset, come to life right before your eyes</p>
            </div>
            <div className="butoons">
            <Button className="boto-primary" variant="contained" endIcon={<ArrowRightAltIcon />}>
              Visit Sttore
            </Button>
            <Button className="boton-secondary" variant="outlined" startIcon={<ExploreIcon />}>
              Explore
            </Button>
            </div>
            <div className="follow-us">
              <h3>FOLLOW US</h3>
              <span><Image src="/imagens/twiter.svg" alt="" width={44} height={44}/></span>
              <span><Image src="/imagens/instagram.svg" alt="" width={44} height={44}/></span>
              <span><Image src="/imagens/discord.svg" alt="" width={44} height={44}/></span>
              <span><Image src="/imagens/facebook.svg" alt="" width={44} height={44}/></span>
            </div>
          </div>
          <div className="image">
              <Image className="image1" src="/imagens/banner.svg" alt=""width={575}height={533} />
            
              <Image className="image2" src="/imagens/paper1.svg" alt="" width={331}height={160} /> 
            
              <Image className="image3" src="/imagens/paper2.svg" alt="" width={137.4}height={84.7}/>
            
              <Image className="image4" src="/imagens/footer-img.svg" alt="" width={566}height={32}/>
            
              <Image className="image5" src="/imagens/elipse-header.svg" alt="" width={488} height={423}/>
            
          </div>
        </div>
      </div>
      <div className="Main">
        <div className="Primera-Seccion">
          <div className="image-main">
              
              <Image className="gafas-rv" src="/imagens/image-rv.svg" alt="" width={522} height={486}/>
            
            
              <Image className="primer-elipse" src="/imagens/elipse1.svg" alt="" width={746.73} height={394.72}/>
            
            
              <Image className="segundo-elipse" src="/imagens/elipse2.svg" alt="" width={451} height={391}/>
            
          </div>
          <div className="pagragram-main">
            <div className="title-inter">
              <h1>Our Virtual Headsets Shine with Unique Features!</h1>
            </div>
            <div className="cajainterna">
              <div >
                <div>
                  <Caja image={"/imagens/estrella2.svg"} paragramph={"High-resolution OLED or LCD screens: Provide sharp and clear visuals."}></Caja>
                </div>
                <div>
                <Caja image={"/imagens/estrella2.svg"} paragramph={"Inside-out tracking: Built-in sensors (cameras or other sensors)."}></Caja>
                </div>
                <div>
                <Caja image={"/imagens/estrella2.svg"} paragramph={"High-resolution OLED or LCD screens: Provide sharp and clear visuals."}></Caja>
                </div>
              </div>
              <div>
                <div>
                <Caja image={"/imagens/estrella2.svg"} paragramph={"Refresh rate: Higher refresh rates reduce motion sickness and provide."}></Caja>
                </div>
                <div>
                <Caja image={"/imagens/estrella2.svg"} paragramph={"Eye tracking: Monitors the movement of the user's eyes, allowing for more."}></Caja>
                </div>
                <div>
                <Caja image={"/imagens/estrella2.svg"} paragramph={"Refresh rate: Higher refresh rates reduce motion sickness and provide."}></Caja>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="Segunda-Seccion">
          <div className="Segunda-Seccion-letras">
            <h1>Jane Wilson, 
            Gaming Creator</h1>  
            <p>A virtual headset creator is an individual or company that specializes in designing and manufacturing virtual reality headsets, also known as VR headsets.</p>
            <Button className="main-button" variant="outlined" endIcon={<ArrowRightAltIcon />}>
            Read my blog
            </Button>
            <div className="ususarios">
              <Image src="/imagens/users.svg" alt="" width={185} height={65} />
              <p>+258K Views</p>
            </div>
          </div>
          <div className="Segunda-Seccion-imagenes">
            <Image className="banner-main-segunda-seccion" src="/imagens/banner-main.svg" alt="" width={688} height={540} />
            <Image className="banner-main-segunda-seccion2" src="/imagens/banner-main2.svg" alt="" width={414} height={282}/>        
          </div>
        </div>
      </div>
      <div className="Footer">
        <div className="footer">
        <div className="Reference">
          <Logo leter={"Holo"} leter2={"Gaze"}></Logo>
          <span><Image src="/imagens/twiter.svg" alt="" width={44} height={44}/></span>
          <span><Image src="/imagens/instagram.svg" alt="" width={44} height={44}/></span>
          <span><Image src="/imagens/discord.svg" alt="" width={44} height={44}/></span>
          <span><Image src="/imagens/facebook.svg" alt="" width={44} height={44}/></span>
          <p>(+994) 199-28-786</p>
        </div>
        <div className="Contact">
          <div className="primer-menu">
            <Lista title={"Menu"} components1={"Home"} components2={"Catalog"} components3={"Features"} components4={"Contact"}></Lista>
          </div>
          <div className="segundo-menu">
            <Lista title={"Company"} components1={"Login"} components2={"Sign Up"} components3={"Privacy"} components4={"Privacy"}></Lista>
          </div>
        </div>
        <div className="loging-footer">
          <h2>Subscribe Our News Letter</h2>
          <p>Sure, please provide your email address to subscribe to newsletter</p>
          <div className="resolution">
          <TextField className="texts" id="outlined-basic" label="Outlined" variant="outlined" />
          <Button className="button-footer" variant="contained">Subscribe</Button>
          </div>
        </div>  
        </div>
        <div className="footer2">
          <Image src="/imagens/line.svg" alt=""  width={1200} height={25}/>
          <p>© 2023 Your Company Name. All rights reserved.</p>          
        </div>
       
      </div>
    </div>
  );
}
